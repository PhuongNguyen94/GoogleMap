//
//  ViewController.swift
//  GoogleMap
//
//  Created by Administrator on 1/26/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController,CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    //MARK: -  An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    
    //MARK: -  The currently selected place.
    var selectedPlace: GMSPlace?

    override func viewDidLoad() {
        super.viewDidLoad()
        GMSServices.provideAPIKey("AIzaSyAMDgnK13ZLsXdeSIPFyqMDfiM66u1l4MA")
        GMSPlacesClient.provideAPIKey("AIzaSyAMDgnK13ZLsXdeSIPFyqMDfiM66u1l4MA")
        
        //MARK : - Create a GMSCameraPosition that tells the map to display the
        //MARK: -  coordinate 10.771367,106.673932 at zoom level 15.
        let camera = GMSCameraPosition.camera(withLatitude: 10.771367, longitude: 106.673932, zoom: 15.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        //MARK: -  Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 10.771367, longitude: 106.673932)
        marker.title = "JV-IT.,JSC"
        marker.snippet = "VietNam"
        marker.map = mapView
   
    }

}

